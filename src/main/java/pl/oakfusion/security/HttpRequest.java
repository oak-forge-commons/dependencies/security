package pl.oakfusion.security;

import java.io.IOException;

public interface HttpRequest {

    String sendRequest(String uri, String headerName, String headerValue) throws IOException, InterruptedException;
}

package pl.oakfusion.security;

public interface SecurityCheck {

    boolean isAuthenticated(String jwt);
}
